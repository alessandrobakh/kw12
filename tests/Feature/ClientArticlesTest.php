<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ClientArticlesTest extends TestCase
{
    /**
     * test can see articles on index page.
     * @group articles
     * @return void
     */
    public function test_see_index_page_with_articles()
    {
        $articles = Article::factory()->count(5)->for(User::factory())->create();

        $response = $this->get('/');
        $response->assertOk();
        foreach ($articles as $article){
            $response->assertSee($article->user->name);
        }
    }

    /**
     * test can see create article page.
     * @group articles
     * @return void
     */
    public function test_can_see_create_article_page()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->get(route('client.articles.create'));
        $response->assertOk();
    }

    /**
     * test can not see create article page.
     * @group articles
     * @return void
     */
    public function test_cannot_see_create_article_page_if_not_auth()
    {
        $response = $this->get(route('client.articles.create'));
        $response->assertRedirect();
    }

    /**
     * test can create article.
     * @group articles
     * @return void
     */
    public function test_can_create_article()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $data = ['body' => $this->faker->realText(200), 'user_id' => $user->id];

        $this->post(route('client.articles.store'), $data);
        $this->assertDatabaseHas('articles', $data);
    }

    /**
     * test can not create article.
     * @group articles
     * @return void
     */
    public function test_can_not_create_article_required()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $data = ['user_id' => $user->id];

        $response = $this->post(route('client.articles.store'), $data);
        $this->assertDatabaseMissing('articles', $data);
        $response->assertSessionHasErrorsIn('body');
    }

    /**
     * test can not create article.
     * @group articles
     * @return void
     */
    public function test_can_not_create_article_min_10()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $data = ['body' => 'asd' ,'user_id' => $user->id];

        $response = $this->post(route('client.articles.store'), $data);
        $this->assertDatabaseMissing('articles', $data);
        $response->assertSessionHasErrorsIn('body');
    }

    /**
     * test can not create article.
     * @group articles
     * @return void
     */
    public function test_can_not_create_article_max_1000()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $data = ['body' => $this->faker->words(1001) ,'user_id' => $user->id];

        $response = $this->post(route('client.articles.store'), $data);
        $this->assertDatabaseMissing('articles', $data);
        $response->assertSessionHasErrorsIn('body');
    }

    /**
     * test can not create article.
     * @group articles
     * @return void
     */
    public function test_can_not_create_article_dont_have_user()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $data = ['body' => $this->faker->words(200) ,'user_id' => 10000];

        $response = $this->post(route('client.articles.store'), $data);
        $this->assertDatabaseMissing('articles', $data);
        $response->assertSessionHasErrors();
    }

    /**
     * test can see article on show page.
     * @group articles
     * @return void
     */
    public function test_see_article_show_page()
    {
        $article = Article::factory()->for(User::factory())->create();

        $response = $this->get(route('client.articles.show', ['article' => $article]));
        $response->assertOk();
        $response->assertSee($article->user->name);
        $response->assertSee($article->body);
    }
}
