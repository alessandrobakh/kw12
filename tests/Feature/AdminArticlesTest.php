<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminArticlesTest extends TestCase
{

    /**
     * @var Collection|Model
     */
    private Collection|Model $admin;

    /**
     * @var Collection|Model
     */
    private Collection|Model $another_user;

    /**
     * @var Collection|Model
     */
    private Collection|Model $articles;

    private $article;

    private $not_approved_article;

    protected function setUp(): void
    {
        parent::setUp();

        $this->admin = User::factory()->create(['is_admin' => true]);
        $this->another_user = User::factory()->create();
        $this->articles = Article::factory(1)->for($this->another_user)->create();
        $this->article = Article::factory()->for($this->another_user)->create();
        $this->not_approved_article = Article::factory()->for($this->another_user)->create(['approved' => false]);
    }

    /**
     * Test admin can see all articles
     * @group admin_articles
     * @return void
     */
    public function test_admin_can_see_all_articles_on_index_page()
    {
        $this->actingAs($this->admin);
        $response = $this->get(route('admin.articles.index'));
        $response->assertOk();
        $response->assertSee($this->another_user->name);
        foreach ($this->articles as $article){
            $response->assertSee($article->body);
        }
    }

    /**
     * Test user cannot enter admin articles index
     * @group admin_articles
     * @return void
     */
    public function test_not_admin_cannot_enter_admin_articles_index()
    {
        $this->actingAs($this->another_user);

        $response = $this->get(route('admin.articles.index'));
        $response->assertRedirect();
    }

    /**
     * Test admin can see edit page
     * @group admin_articles
     * @return void
     */
    public function test_admin_can_see_edit_page()
    {
        $this->actingAs($this->admin);
        $response = $this->get(route('admin.articles.edit', $this->article));
        $response->assertOk();
    }

    /**
     * Test admin can see edit page
     * @group admin_articles
     * @return void
     */
    public function test_admin_can_delete_article()
    {
        $this->actingAs($this->admin);

        $this->delete(route('admin.articles.destroy', $this->article));
        $this->assertDatabaseMissing('articles', ['body' => $this->article->body]);
    }
}
