<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ifUserAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (\Auth::user() and \Auth::user()->is_admin) {
            return $next($request);
        }

        return redirect('/');
    }
}
