<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Article extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = ['body', 'user_id', 'publication_date', 'approved'];

    /**
     * @var string[]
     */
    protected $casts = [
        'publication_date' => 'datetime',
        'approved' => 'boolean',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
