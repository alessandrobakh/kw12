@extends('layouts.app')

@section('content')

    <div class="col mb-2">
        <div class="card h-100">
            <div class="card-body">
                <h5 class="card-title">{{$article->user->name}}</h5>
                <p class="card-text">
                    {{$article->body}}
                </p>
                <small class="text-muted">Create date: {{$article->created_at->format('d M Y - H:i')}}</small>
            </div>
            <div class="card-footer">
                <small class="text-muted">Publication date: {{$article->publication_date->format('d M Y - H:i')}}</small>
            </div>
        </div>
    </div>

    <a class="btn btn-outline-primary" href="{{route('client.articles.index')}}">Go back</a>

@endsection
