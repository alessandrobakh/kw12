@extends('layouts.app')

@section('content')

    <h1>All articles</h1>

    @auth()
        <a class="btn btn-outline-primary mb-2" href="{{route('client.articles.create')}}">Create new article</a>
    @endauth

    @if($articles->count() > 0)

        <div class="row row-cols-1 row-cols-md-3 g-4">
        @foreach($articles as $article)

                <div class="col mb-2">
                    <div class="card h-100">
                        <div class="card-body">
                            <h5 class="card-title">{{$article->user->name}}</h5>
                            <p class="card-text text-truncate">
                                <a href="{{route('client.articles.show', ['article' => $article])}}">{{$article->body}}</a>
                            </p>
                            <small class="text-muted">Create date: {{$article->created_at->format('d M Y - H:i')}}</small>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Publication date: {{$article->publication_date->format('d M Y - H:i')}}</small>
                        </div>
                    </div>
                </div>

        @endforeach
    </div>

        <div class="row justify-content-md-center pt-5">
            <div class="col-md-auto">
                {{ $articles->withQueryString()->links('pagination::bootstrap-4') }}
            </div>
        </div>

    @else

    <p>No articles</p>

    @endif

@endsection
