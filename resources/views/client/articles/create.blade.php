@extends('layouts.app')

@section('content')

    <form method="POST" action="{{route('client.articles.store')}}">
        @csrf
        <div class="form-group">
            <label for="body"><b>Article text</b></label>
            <textarea class="form-control" name="body" id="body" cols="30" rows="10" placeholder="Write here..."></textarea>
            @error('body')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary mt-2">Save</button>
    </form>

@endsection
