@extends('layouts.admin')

@section('content')

    <h1>All articles</h1>

    @if($articles->count() > 0)

        <table class="table table-dark table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Author</th>
                <th scope="col">Body</th>
                <th scope="col">Created at</th>
                <th scope="col">Published at</th>
                <th scope="col">Approved</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($articles as $article)
            <tr>
                <th scope="row">{{$article->id}}</th>
                <td>{{$article->user->name}}</td>
                <td>
                        <span class="d-inline-block text-break">
                            {{$article->body}}
                        </span>
                </td>
                <td>{{$article->created_at->format('d M Y - H:i')}}</td>
                <td>
                    @if(!is_null($article->publication_date))
                    {{$article->publication_date->format('d M Y - H:i')}}
                    @else
                        <p>Not published</p>
                    @endif
                </td>
                <td>
                    @if($article->approved)
                    Yes
                    @else
                    check edit to approve
                    @endif
                </td>
                <td>
                    <div class="btn-group">
                    <a class="btn btn-sm btn-outline-warning" type="button" href="{{route('admin.articles.edit', $article)}}">Edit</a>
                        <form method="POST" action="{{route('admin.articles.destroy', $article)}}">
                            @csrf
                            @method('delete')
                            <button class="btn btn-sm btn-outline-danger" type="submit">Delete</button>
                        </form>
                    </div>

                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

        <div class="row justify-content-md-center p-5">
            <div class="col-md-auto">
                {{ $articles->links('pagination::bootstrap-4') }}
            </div>
        </div>

    @else

    <p>No articles</p>

    @endif

@endsection
