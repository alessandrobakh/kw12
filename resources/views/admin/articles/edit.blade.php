@extends('layouts.admin')

@section('content')

    <form method="POST" action="{{route('admin.articles.update', ['article' => $article])}}">
        @csrf
        @method('put')
        <div class="form-group">
            <h4>{{$article->user->name}}</h4>
            <label for="body"><b>Article text</b></label>
            <textarea class="form-control" name="body" id="body" cols="30" rows="10" placeholder="Write here...">{{$article->body}}</textarea>
            @error('body')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
        </div>

        @if(!$article->approved)
        <div class="form-group">
            <label for="approved"><b>Approve this article?</b></label>
            <select class="form-control" name="approved" id="approved">
                <option value="1">Yes</option>
                <option value="0">No</option>
            </select>
            @error('approved')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
        </div>
        @endif

        @if(is_null($article->publication_date))
        <div class="form-group">
            <label for="publication_date">Set date:</label>
            <input class="form-check" type="date" name="publication_date" id="publication_date">
            @error('publication_date')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
        </div>
        @endif

        <button type="submit" class="btn btn-primary mt-2">Save</button>
    </form>

@endsection
