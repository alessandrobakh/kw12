<?php

use App\Http\Controllers\ArticlesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [ArticlesController::class, 'index'])->name('home');

Route::prefix('client')->name('client.')->group(function (){
    Route::resource('articles', ArticlesController::class)
        ->only(['create', 'store'])->middleware('auth');
    Route::resource('articles', ArticlesController::class)
        ->only(['index', 'show']);
});

Route::prefix('admin')->name('admin.')->middleware('ifadmin')->group(function (){
    Route::resource('articles', \App\Http\Controllers\Admin\ArticlesController::class)
        ->except(['create', 'store', 'show']);
});
